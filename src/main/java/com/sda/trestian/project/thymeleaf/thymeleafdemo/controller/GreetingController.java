package com.sda.trestian.project.thymeleaf.thymeleafdemo.controller;

import com.sda.trestian.project.thymeleaf.thymeleafdemo.model.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping("/greeting")
public class GreetingController {
// PathVariable , RequestParam , ModelAttribute

    @PostMapping("/create-user")
    public String createUser(@ModelAttribute User user){
        System.out.println(user);
        // userService.createUser(user); // userRepository.save(user);
        return "usercreated";
    }

    @GetMapping("/hello")
    public String sayHello(@RequestParam boolean isAdmin, Model model){
        //userService.getAllUsers()
        List<User> users = new ArrayList<>();
        User user = new User();
        user.setAddress(Arrays.asList("Brasov", "Cluj"));
        user.setAge(23);
        user.setName("Cristi");

        User user2 = new User();
        user2.setAddress(Arrays.asList("Budapesta", "Cluj"));
        user2.setAge(36);
        user2.setName("Dan");
        users.add(user);
        users.add(user2);
        User user3 = new User();
        user3.setAge(34);
        user3.setName("Paul");
        users.add(user3);
        model.addAttribute("users",users);

        List<String> countries = new ArrayList<>();
        countries.add("Romania");
        countries.add("Bulgaria");
        countries.add("Croatia");
        model.addAttribute("countries", countries);

        model.addAttribute("welcomeMessage", "Welcome Paul");
        model.addAttribute("customerAge",22);
        model.addAttribute("user",new User());
        model.addAttribute("isAdmin", isAdmin);
        return "welcome"; // -> adds SUFIX "/templates/" {} si adauga PREFIXUL ".html"
        // /templates/welcome.html
    }
}
